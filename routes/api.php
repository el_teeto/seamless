<?php

use Illuminate\Http\Request;
use  App\Jobs\CourseJob;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('recover', 'AuthController@recover');

Route::group(['middleware'=>'jwt.auth'],function(){

    Route::get('create-courses',function(){
        // $job = (new App\Jobs\CourseJob)
        //         ->delay(Carbon::now()->addSeconds(5));
        //        dispatch($job);
            dispatch(new App\Jobs\CourseJob);
            return 'success';
    });
    //Route::get('course-reg','CourseRegController@create');
    Route::post('course-reg','CourseRegController@store');
    Route::get('courses','CourseRegController@show');
    Route::get('export-courses','CourseRegController@export');
});

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'AuthController@logout');

    Route::get('test', function(){
        return response()->json(['foo'=>'bar']);
    });
});