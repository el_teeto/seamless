<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text'=> $faker->text(100),
        //'course_title' => $faker->title,
        'course_code'=>$faker->word,
         'course_unit'=>$faker->randomDigit,
         'description' => $faker->text(100)
    ];
});
