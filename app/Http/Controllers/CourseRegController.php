<?php

namespace App\Http\Controllers;

use App\CourseReg;
use App\Course;
use Excel;
use App\User;
use Illuminate\Http\Request;

class CourseRegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendResponse($result, $message)
    {
    	$response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }
    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Courses::all();
        if(!$courses){
            return $this->sendError('can not load courses data');
        }else{
            return $this->sendResponse($courses,'success');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            
            'user_id'=> 'required',
            'course_id'=> 'required',

         ]);
        // return $request;
         for($i=0;$i<count( $request['course_id']);$i++){
           //return 
            $course = new CourseReg;
            $course->user_id = $request->user_id;
            $course->course_id = $request->course_id[$i];
         
            $course->save();
         }
            return $this->sendResponse(null,'Course Registration Complete');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseReg  $courseReg
     * @return \Illuminate\Http\Response
     */
    public function show(CourseReg $courseReg)
    {
        $courses = Course::with('courseReg')->get();
         return $courses;
    }

    public function export(){
         
        $filename = 'file_'.uniqid();
          $courses = Course::all();
           foreach($courses as $course){
               $data[] = [
                   'SN' => $course->id,
                   'Text' => $course->text,
                   'Course Code' => $course->course_code,
                   'Course Unit' => $course->course_unit,
                   'Course title' => $course->description,
                  
               ];
           }
            return Excel::create($filename,function($excel) use ($data) {
                $excel->sheet('Courses Sheet', function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->export('xls');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseReg  $courseReg
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseReg $courseReg)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CourseReg  $courseReg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseReg $courseReg)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseReg  $courseReg
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseReg $courseReg)
    {
        //
    }
}
