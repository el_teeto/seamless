<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseReg extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function courses(){
        $this->belongsTo(Course::class);
    }
  
}
