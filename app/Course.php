<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Course extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function courseReg(){
        return $this->hasMany('App\CourseReg')->where('user_id', Auth::id());
    }
}
